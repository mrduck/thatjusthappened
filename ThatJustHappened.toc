## Interface: 50400
## Title: ThatJustHappened
## Reposted and updated to 5.4.2 by Boëndal@EU.Frostmane 20131212
## Reposted by: Shadowyn@US.Mal'Ganis
## Updated to 5.0.4 by shadowyn@US.Mal'Ganis 20120829
## Updated to 4.2.1 by shadowyn@US.Mal'Ganis 20110717
## Updated to 4.2 by shadowyn@US.Mal'Ganis 20110628
## Updated to 4.1 by shadowyn@US.Mal'Ganis
## Updated to 4.0 by dukka@EU.Shadowsong
## Additional Bugfixes and enhancements by MopA
##
## Version: 4.0
## Original Author: Belleboom@Earthen Ring
## Version: 3.0.5.2
## Notes: Event announcer... shake and bake!
## SavedVariablesPerCharacter: TJH_Rules, TJH_Options

ThatJustHappened.xml
ThatJustHappened.lua
