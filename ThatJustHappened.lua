-- Notes:
--
-- GUILD is a valid channel but undocumented as it probably shouldn't be used (unless you want to be /gkicked? spam potential with this mod is very high:p)

--------------------------
-- Variables, Defined --
--------------------------
local orange =	"|cffffaa11";
local red = "|cFFFF0000";
local yellow = "|cFFFFFF00";
local cyan = "|cFF00FFFF";
local white = "|cFFFFFFFF";
local TJH_delrule = "";
local TJH_eventType = "";	-- event to watch
local TJH_sourceName = "";	-- source of event
local TJH_destName = "";	-- target of event
local TJH_spellName = "";	-- spell name to watch for
local TJH_stacks = "";	-- stacks to watch for
local TJH_affFlag = "";	-- self/party/raid/friend/enemy/target/focus
local TJH_powerType = "";
local TJH_spellSchool = "";
--local TJH_timeDelay = 0; -- temporary until rules input is set up
TJH_Channel = "SELF";	-- output channel
TJH_ruleSpam = "";
TJH_Rules = {};
TJH_whisperTarget = UnitName("player"); -- defaults to player if whisper is activated to avoid unwanted spam
TJH_Options = {enabled=0};
--TJH_Profiles = {default=1};
--TJH_ActiveProfile = default;
--local profile = TJH_ActiveProfile;

-- affiliation/type filters below
local bit_bor = _G.bit.bor;
local bit_band = _G.bit.band;
local string_gsub = string.gsub;
local string_sub = string.sub;
local string_find = string.find;
local CombatLog_Object_IsA = CombatLog_Object_IsA;

--local COMBATLOG_FILTER_MY_PET = bit_bor(
--	COMBATLOG_OBJECT_AFFILIATION_MINE,
--	COMBATLOG_OBJECT_REACTION_FRIENDLY,
--	COMBATLOG_OBJECT_CONTROL_PLAYER,
--	COMBATLOG_OBJECT_TYPE_GUARDIAN,
--	COMBATLOG_OBJECT_TYPE_PET
--	);
local COMBATLOG_FILTER_MINE = bit_bor(
	COMBATLOG_OBJECT_AFFILIATION_MINE,
	COMBATLOG_OBJECT_REACTION_FRIENDLY,
	COMBATLOG_OBJECT_CONTROL_PLAYER,
	COMBATLOG_OBJECT_TYPE_PLAYER,
	COMBATLOG_OBJECT_TYPE_OBJECT
	);
local COMBATLOG_FILTER_FRIENDLY_UNITS = bit_bor(
	COMBATLOG_OBJECT_AFFILIATION_MINE,
	COMBATLOG_OBJECT_AFFILIATION_PARTY,
	COMBATLOG_OBJECT_AFFILIATION_RAID,
	COMBATLOG_OBJECT_AFFILIATION_OUTSIDER,
	COMBATLOG_OBJECT_REACTION_FRIENDLY,
	COMBATLOG_OBJECT_CONTROL_PLAYER,
	COMBATLOG_OBJECT_CONTROL_NPC,
	COMBATLOG_OBJECT_TYPE_PLAYER,
	COMBATLOG_OBJECT_TYPE_NPC,
	COMBATLOG_OBJECT_TYPE_PET,
	COMBATLOG_OBJECT_TYPE_GUARDIAN,
	COMBATLOG_OBJECT_TYPE_OBJECT
	);
local COMBATLOG_FILTER_HOSTILE_UNITS = bit_bor(
--	COMBATLOG_OBJECT_REACTION_NEUTRAL,
	COMBATLOG_OBJECT_REACTION_HOSTILE,
	COMBATLOG_OBJECT_CONTROL_PLAYER,
	COMBATLOG_OBJECT_CONTROL_NPC,
	COMBATLOG_OBJECT_TYPE_PLAYER,
	COMBATLOG_OBJECT_TYPE_NPC,
	COMBATLOG_OBJECT_TYPE_PET,
	COMBATLOG_OBJECT_TYPE_GUARDIAN,
	COMBATLOG_OBJECT_TYPE_OBJECT
	);
local COMBATLOG_FILTER_PARTY = bit_bor(
	COMBATLOG_OBJECT_AFFILIATION_MINE,
	COMBATLOG_OBJECT_AFFILIATION_PARTY,
	COMBATLOG_OBJECT_REACTION_FRIENDLY,
	COMBATLOG_OBJECT_CONTROL_PLAYER,
	COMBATLOG_OBJECT_TYPE_PLAYER,
	COMBATLOG_OBJECT_TYPE_PET,
	COMBATLOG_OBJECT_TYPE_GUARDIAN,
	COMBATLOG_OBJECT_TYPE_OBJECT
	);
local COMBATLOG_FILTER_RAID = bit_bor(
	COMBATLOG_OBJECT_AFFILIATION_MINE,
	COMBATLOG_OBJECT_AFFILIATION_PARTY,
	COMBATLOG_OBJECT_AFFILIATION_RAID,
	COMBATLOG_OBJECT_REACTION_FRIENDLY,
	COMBATLOG_OBJECT_CONTROL_PLAYER,
	COMBATLOG_OBJECT_TYPE_PLAYER
--	,
--	COMBATLOG_OBJECT_TYPE_PET,
--	COMBATLOG_OBJECT_TYPE_GUARDIAN,
--	COMBATLOG_OBJECT_TYPE_OBJECT
	);
local COMBATLOG_OBJECT_TARGET = COMBATLOG_OBJECT_TARGET;
local COMBATLOG_OBJECT_FOCUS = COMBATLOG_OBJECT_FOCUS;


--------------------------
-- Functions --
--------------------------

function TJH_OnLoad(frame)
	--define slash command parsing
 	SlashCmdList["ThatJustHappened"] = TJH_SlashCMD;
 	SLASH_ThatJustHappened1 = "/tjh";
 	SLASH_ThatJustHappened2 = "/thatjusthappened";
	--register all variable and combat events to the frame
	frame:RegisterEvent("VARIABLES_LOADED");
	frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
end

-- slash handler
function TJH_SlashCMD(msg)
	local msg1, msg2 = strsplit(";",msg);
	local msg_lower = strlower(msg1);
--	help text
	if (msg_lower=="gui" or msg_lower == "") then TJH_MainForm:Show(); return; end
	if (msg_lower == "help") then
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh"..orange.." or "..white.."/tjh gui"..orange.." for GUI");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh help"..orange.." for this list");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh on"..orange.." to activate");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh off"..orange.." to deactivate");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh rule EVENT_NAME;Sourcename;TargetName;SpellName;\nAffiliation;Channel;Spam;Delay"..orange.." (leave blank to see all of a type, ex: /tjh rule INTERRUPT;;;;;;); affiliation: self/party/raid/friend/enemy/target/focus");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh list"..orange.." to list current rules");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh delete"..orange.." to delete all rules");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh delete;3"..orange.." to delete the third rule");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh report"..orange.." to print basic settings");
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."/tjh reset"..orange.." deletes all rules, resets output to SELF");
--	toggle on/off
	elseif (msg_lower == "on") then
		TJH_Options.enabled = 1;
		TJH_EnableButton();
		TJH_Print(orange.."TJH enabled.","SELF");
	elseif (msg_lower == "off") then
		TJH_Options.enabled = 0;
		TJH_EnableButton();
		self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
		TJH_Print(orange.."TJH disabled.","SELF");
	elseif (msg_lower == "report") then
		TJH_ListRules();
	elseif (msg_lower == "list") then
		TJH_ListRules();
	elseif (string_sub(strlower(msg),1,7) == "delete;") then
		TJH_delrule = string_sub(msg,8);
		TJH_DeleteRule(TJH_delrule);
	elseif msg_lower == "delete" then
		TJH_ClearRules();
	elseif (msg_lower == "reset") then
		TJH_Channel = "SELF";
		TJH_ClearRules();
		TJH_Print(orange.."TJH: No rules defined.", "SELF");
--	rule setting code
--	elseif (string_sub(strlower(msg),1,3) == "ssc") then
--		-- Add a default set of rules
--		TJH_addRule("", "Lurker", "", "Spout", "", "RAID", "");
--		TJH_addRule("SUMMON", "Greyheart Tidecaller", "", "Water Elemental Totem", "", "RW", "Kill the Totem!");
--		TJH_addRule("AURA_APPLIED", "", "", "Spell Reflection", "", "SELF", "");
--		TJH_addRule("DAMAGE", "", "", "", "Tidal Wave", "", "SELF");
--		TJH_addRule("SUMMON", "Fathom-Guard Tidalvess", "", "", "Spitfire Totem", "", "RW");
	elseif (string_sub(strlower(msg),1,5) == "rule ") then
		local TJH_ruleString = string_gsub(string_sub(msg,6),"[%[%]]","");
		local TJH_eventType, TJH_sourceName, TJH_destName, TJH_spellName, TJH_affFlag, TJH_Channel, TJH_ruleSpam, TJH_timeDelay, TJH_stacks = strsplit(";",TJH_ruleString);
		if TJH_eventType == nil then TJH_eventType = ""; end
		if TJH_sourceName == nil then TJH_sourceName = ""; end
		if TJH_destName == nil then TJH_destName = ""; end
		if TJH_spellName == nil then TJH_spellName = ""; end
		if TJH_affFlag == nil then TJH_affFlag = ""; end
		if TJH_Channel == nil then TJH_Channel = "SELF"; end
		if TJH_ruleSpam == nil then TJH_ruleSpam = ""; end
		if TJH_timeDelay == nil then TJH_timeDelay = ""; end
		if TJH_stacks == nil then TJH_stacks = ""; end
		-- will set source/target to target or focus; if you want it to update to current target when you change target/focus use "target" or "focus" affiliation option
		if TJH_sourceName == "target" then if UnitName("target")~= nil then TJH_sourceName = UnitName("target") else TJH_Print(orange.."TJH: You don't have a target!", "SELF"); end end
		if TJH_sourceName == "focus" then if UnitName("focus")~= nil then TJH_sourceName = UnitName("focus") else TJH_Print(orange.."TJH: You don't have a focus!", "SELF"); end end
		if TJH_destName == "target" then if UnitName("target")~= nil then TJH_destName = UnitName("target") else TJH_Print(red.."TJH: You don't have a target!", "SELF"); end end
		if TJH_destName == "focus" then if UnitName("focus")~= nil then TJH_destName = UnitName("focus") else TJH_Print(red.."TJH: You don't have a focus!", "SELF"); end end
		if TJH_affFlag ~= ("self" or "party" or "raid" or "friend" or "enemy" or "target" or "focus" or "") then DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..orange.."Affiliation: self/party/raid/friend/enemy/target/focus"); end

		TJH_addRule(strupper(strtrim(TJH_eventType)), strtrim(TJH_sourceName), strtrim(TJH_destName), strtrim(TJH_spellName), strtrim(TJH_affFlag), strtrim(TJH_Channel), TJH_ruleSpam, strtrim(TJH_timeDelay), strtrim(TJH_stacks));
	else
	end
end


-- OnEvent, where most of the work takes place
function TJH_OnEvent(frame, event, ...)
	-- check if the mod is disabled, if so return
	if TJH_Options.enabled == 0 then return; end

	if event == "VARIABLES_LOADED" then
		if(not TJH_eventType) 	then TJH_eventType = ""; 	end;
		if(not TJH_sourceName) 	then TJH_sourceName = ""; 	end;
		if(not TJH_destName) 	then TJH_destName = ""; 	end;
		if(not TJH_spellName) 	then TJH_spellName = ""; 	end;
		if(not TJH_affFlag) 	then TJH_affFlag = ""; 		end;
		if(not TJH_Channel) 	then TJH_Channel = "SELF"; 	end;
		if(not TJH_ruleSpam) 	then TJH_ruleSpam = ""; 	end;
		if(not TJH_timeDelay) 	then TJH_timeDelay = ""; 	end;
		if(not TJH_stacks) 	    then TJH_stacks = ""; 		end;
	end

	local playerid = UnitGUID("player");
	local targetid = UnitGUID("target");

--		arguments:
--		1: timestamp
--	    2: eventType
--      3: hideCaster
--	    4: sourceGUID
--	    5: sourceName
--      6: sourceFlags
--      7: sourceRaidFlags
--      8: destGUID
--      9: destName
--     10: destFlags
--     11: destRaidFlags
--     12: spellID
--     13: spellName
--     14: spellSchool
--     15: Miss
--     16: Stacks
--     17: Absorbed
--     18: Critical

	if (select(2, ...) ~= nil)  then argEvent 		= select(2, ...);  end
	if (select(5, ...) ~= nil)  then argSourceName  = select(5, ...);  end
	if (select(6, ...) ~= nil)  then argSourceFlags = select(6, ...);  end
	if (select(9, ...) ~= nil)  then argDestName 	= select(9, ...);  end
	if (select(10, ...) ~= nil)  then argDestFlags 	= select(10, ...);  end
	if (select(12, ...) ~= nil) then argSpellID 	= select(12, ...); end
	if (select(13, ...) ~= nil) then argSpellName 	= select(13, ...); end
	if (select(14, ...) ~= nil) then argSchool 		= select(14, ...); end
	if (select(15, ...) ~= nil) then argMiss 		= select(15, ...); end

	-- MopA: http://maintankadin.failsafedesign.com/forum/index.php?f=9&t=10819&rb_v=viewtopic&start=390#p635397

	if (select(16, ...) ~= nil) then argStacks = select(16, ...); end
	if (select(17, ...) ~= nil) then argAbsorbed = select(17, ...); end
	if (select(18, ...) ~= nil) then argCritical = select(18, ...); end

	if argEvent == nil 		then argEvent 		= ""; end
	if argSourceName == nil then argSourceName 	= ""; end
	if argDestName == nil 	then argDestName 	= ""; end
	if argSpellID == nil 	then argSpellID 	= ""; end
	if argSpellName == nil 	then argSpellName 	= ""; end
	if argMiss == nil 		then argMiss 		= ""; end
	if argStacks == nil 	then argStacks 		= ""; end
	if argAbsorbed == nil 	then argAbsorbed 	= ""; end
	if argCritical == nil 	then argCritical 	= ""; end

	local rules=#(TJH_Rules);	--the table will be iterated only once
	for i=1,rules do
		msg=TJH_Rules[i];
		TJH_eventType, TJH_sourceName, TJH_destName, TJH_spellName, TJH_affFlag, TJH_Channel, TJH_ruleSpam, TJH_timeDelay, TJH_stacks = strsplit(";", msg);

		if TJH_affFlag == "self" then
			if (CombatLog_Object_IsA(argSourceFlags, COMBATLOG_FILTER_MINE) or CombatLog_Object_IsA(argDestFlags, COMBATLOG_FILTER_MINE)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "party" then
			if (CombatLog_Object_IsA(argSourceFlags, COMBATLOG_FILTER_PARTY) or CombatLog_Object_IsA(argDestFlags, COMBATLOG_FILTER_PARTY)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "raid" then
			if (CombatLog_Object_IsA(argSourceFlags, COMBATLOG_FILTER_RAID) or CombatLog_Object_IsA(argDestFlags, COMBATLOG_FILTER_RAID)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "friend" then
			if ((argSourceFlags ~= nil) and (bit_band(argSourceFlags, COMBATLOG_OBJECT_REACTION_FRIENDLY) == COMBATLOG_OBJECT_REACTION_FRIENDLY)) or ((argDestFlags ~= nil) and (bit_band(argDestFlags, COMBATLOG_OBJECT_REACTION_FRIENDLY) == COMBATLOG_OBJECT_REACTION_FRIENDLY)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "enemy" then
			if ((argSourceFlags ~= nil) and (bit_band(argSourceFlags, COMBATLOG_OBJECT_REACTION_HOSTILE) == COMBATLOG_OBJECT_REACTION_HOSTILE)) or ((argDestFlags ~= nil) and (bit_band(argDestFlags, COMBATLOG_OBJECT_REACTION_HOSTILE) == COMBATLOG_OBJECT_REACTION_HOSTILE)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "target" then
			if (CombatLog_Object_IsA(argSourceFlags, COMBATLOG_OBJECT_TARGET) or CombatLog_Object_IsA(argDestFlags, COMBATLOG_OBJECT_TARGET)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "focus" then
			if (CombatLog_Object_IsA(argSourceFlags, COMBATLOG_OBJECT_FOCUS) or CombatLog_Object_IsA(argDestFlags, COMBATLOG_OBJECT_FOCUS)) then
				TJH_EventScan();
			end
		elseif TJH_affFlag == "" then
			TJH_EventScan();
		end
	end
end


function TJH_EventScan()
	if (TJH_eventType ~= nil) and (TJH_sourceName ~= nil) and (TJH_destName ~= nil) and (TJH_spellName ~= nil) then
		if (string_find(argEvent, TJH_eventType) ~= nil) and (string_find(argSourceName, TJH_sourceName) ~= nil) and (string_find(argDestName, TJH_destName) ~= nil) and ((string_find(argSpellName, TJH_spellName) ~= nil) or (string_find(argSpellID, TJH_spellName) ~= nil)) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_eventType ~= nil) and (TJH_sourceName ~= nil) and (TJH_destName ~= nil) then
		if (string_find(argEvent, TJH_eventType) ~= nil) and (string_find(argSourceName, TJH_sourceName) ~= nil) and (string_find(argDestName, TJH_destName) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_sourceName ~= nil) and (TJH_destName ~= nil) and (TJH_spellName ~= nil) then
		if (string_find(argSourceName, TJH_sourceName) ~= nil) and (string_find(argDestName, TJH_destName) ~= nil) and ((string_find(argSpellName, TJH_spellName) ~= nil) or (string_find(argSpellID, TJH_spellName) ~= nil)) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_eventType ~= nil) and (TJH_sourceName ~= nil) then
		if (string_find(argEvent, TJH_eventType) ~= nil) and (string_find(argSourceName, TJH_sourceName) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_eventType ~= nil) and (TJH_destName ~= nil) then
		if (string_find(argEvent, TJH_eventType) ~= nil) and (string_find(argDestName, TJH_destName) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_eventType ~= nil) and (TJH_spellName ~= nil) then
		if (string_find(argEvent, TJH_eventType) ~= nil) and ((string_find(argSpellName, TJH_spellName) ~= nil) or (string_find(argSpellID, TJH_spellName) ~= nil)) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_sourceName ~= nil) and (TJH_destName ~= nil) then
		if (string_find(argSourceName, TJH_sourceName) ~= nil) and (string_find(argDestName, TJH_destName) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_sourceName ~= nil) and (TJH_spellName ~= nil) then
		if (string_find(argSourceName, TJH_sourceName) ~= nil) and ((string_find(argSpellName, TJH_spellName) ~= nil) or (string_find(argSpellID, TJH_spellName) ~= nil)) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_destName ~= nil) and (TJH_spellName ~= nil) then
		if (string_find(argDestName, TJH_destName) ~= nil) and ((string_find(argSpellName, TJH_spellName) ~= nil) or (string_find(argSpellID, TJH_spellName) ~= nil)) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_eventType ~= nil) then
		if (string_find(argEvent, TJH_eventType) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_sourceName ~= nil) then
		if (string_find(argSourceName, TJH_sourceName) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_destName ~= nil) then
		if (string_find(argDestName, TJH_destName) ~= nil) then
			 TJH_SpammitySpam();
		end
	elseif (TJH_spellName ~= nil) then
		if (string_find(argSpellName, TJH_spellName) ~= nil) then
			 TJH_SpammitySpam();
		end
	else
		 TJH_SpammitySpam();
	end
end


function TJH_SpammitySpam()

	local argLogic = "";
	local junk = "";
	local doSpam = 0;

	TJH_Channel = string_gsub(TJH_Channel, "*SOURCE", argSourceName);
	TJH_Channel = string_gsub(TJH_Channel, "*TARGET", argDestName);
	if TJH_ruleSpam ~= "" then
		TJH_ruleSpam = string_gsub(TJH_ruleSpam, "*event", argSpellName);
		TJH_ruleSpam = string_gsub(TJH_ruleSpam, "*source", argSourceName);
		TJH_ruleSpam = string_gsub(TJH_ruleSpam, "*target", argDestName);
		TJH_ruleSpam = string_gsub(TJH_ruleSpam, "*spell", argSpellName);
		TJH_ruleSpam = string_gsub(TJH_ruleSpam, "*fail", argMiss);
		TJH_wait(TJH_timeDelay,TJH_Print,TJH_ruleSpam, TJH_Channel);
	return;
	end

	if (argEvent == "SPELL_CAST_START") then
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." begins casting "..argSpellName..".", TJH_Channel);
		return;
	elseif (argSpellName ~= nil and argSpellName ~= 1 and (string_find(argEvent, "ENCHANT") ~= nil)) then
		if (string_find(argEvent, "ENCHANT_APPLIED") ~= nil) then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." casts "..argSpellID.." on "..argDestName.."'s "..argSchool..".", TJH_Channel);
		elseif (string_find(argEvent, "ENCHANT_REMOVED")) then
			TJH_wait(TJH_timeDelay,TJH_Print,argSpellID.." fades from "..argDestName.."'s "..argSchool..".", TJH_Channel);
		end
		return;
	elseif (argEvent == "PARTY_KILL") then
		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." killed by "..argSourceName.."!", TJH_Channel);
		return;
	elseif (argEvent == "INSTAKILL") then
		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." insta-killed by "..argSourceName.."!", TJH_Channel);
		return;
	elseif (argEvent == "UNIT_DIED") then
		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." died.", TJH_Channel);
		return;
	elseif (argEvent == "UNIT_DESTROYED") then
		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." destroyed!", TJH_Channel);
		return;
	elseif (string_find(argEvent, "HEAL") ~= nil) then
		if (argEvent == "SPELL_HEAL") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." heals "..argDestName.." for "..argMiss..".", TJH_Channel);
		elseif (argEvent == "SPELL_PERIODIC_HEAL") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." heals "..argDestName.." for "..argMiss..".", TJH_Channel);
		end
		return;
	elseif string_find(argEvent, "FAILED") then
		if (argEvent == "SPELL_CAST_FAILED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." failed: "..argMiss, TJH_Channel);
		elseif (argEvent == "SPELL_DISPEL_FAILED") then	-- check to make sure all such events have destinations
			TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." failed to dispel "..argDestName..".", TJH_Channel);
		end
		return;
	elseif string_find(argEvent, "INTERRUPT") then
		-- Original
		-- argDestName:target of interupt,  argStacks:spell name(enemy) , argSourceName:source , argSpellName:innterupt spell name
		-- MopA Mod: http://maintankadin.failsafedesign.com/forum/index.php?f=9&t=10819&rb_v=viewtopic&start=405#p645055
		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.."'s "..argStacks.." ["..argSpellName.."] by "..argSourceName..".", TJH_Channel);
		return;
	elseif (argEvent == "SPELL_EXTRA_ATTACKS") then
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." gains "..argMiss.." extra attack(s) from "..argSpellName..".", TJH_Channel);
		return;
	elseif (string_find(argEvent, "DAMAGE") ~= nil) then
		if (string_find(argEvent, "MISSED") == nil) then
			if (argEvent == "SWING_DAMAGE") then
				TJH_SchoolTypeParser();
				TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.. " does "..argSpellID.." "..TJH_spellSchool.." damage to "..argDestName..".", TJH_Channel);
			elseif (argEvent == "SPELL_DAMAGE") then
				TJH_SchoolTypeParser();
				TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.. "'s "..argSpellName.." does "..argMiss.." "..TJH_spellSchool.." damage to "..argDestName..".", TJH_Channel);
			elseif (argEvent == "RANGE_DAMAGE") then
				TJH_SchoolTypeParser();
				TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.. "'s "..argSpellName.." does "..argMiss.." "..TJH_spellSchool.." damage to "..argDestName..".", TJH_Channel);
			elseif (argEvent == "SPELL_PERIODIC_DAMAGE") then
				TJH_SchoolTypeParser();
				TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.. "'s "..argSpellName.." does "..argMiss.." "..TJH_spellSchool.." damage to "..argDestName..".", TJH_Channel);
			elseif (argEvent == "DAMAGE_SHIELD") then
				TJH_SchoolTypeParser();
				TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.. "'s "..argSpellName.." does "..argMiss.." "..TJH_spellSchool.." damage to "..argDestName..".", TJH_Channel);
			elseif (argEvent == "ENVIRONMENTAL_DAMAGE") then
				TJH_wait(TJH_timeDelay,TJH_Print,argDestName.. " suffers "..argSpellName.." "..strlower(argSpellID).." damage.", TJH_Channel);
			elseif (argEvent == "SPELL_BUILDING") then
				TJH_wait(TJH_timeDelay,TJH_Print,argDestName.. " suffers "..argSpellName.." "..strlower(argSpellID).." damage.", TJH_Channel);
			end
		end
		return;
	-- double-check the output for these in the log
	elseif (argEvent == "SPELL_DISPEL") then
		TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." dissipates from "..argDestName..".", TJH_Channel);
	elseif (argEvent == "SPELL_STOLEN") then
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." steals the "..argStacks.." "..strlower(argCritical)..".", TJH_Channel);

	elseif (string_find(argEvent, "AURA") ~= nil) then
		if (string_find(argEvent, "AURA_APPLIED") ~= nil) then

		-- 20110717 bkw: added stack conditional
		-- 20110720 bkw: added >, <, <>, >=, <=, =, (blank) conditionals
		-- 20110721 bkw: also !=

		if (TJH_stacks == nil) then TJH_stacks = ""; end
		argLogic = "";

		if (strfind(TJH_stacks, ">=") ~= nil) then
			argLogic = ">=";
			junk, TJH_stacks = strsplit("=",TJH_stacks);
		elseif (strfind(TJH_stacks, "<=") ~= nil) then
			argLogic = "<=";
			junk, TJH_stacks = strsplit("=",TJH_stacks);
		elseif (strfind(TJH_stacks, "<>") ~= nil) then
			argLogic = "<>";
			junk, TJH_stacks = strsplit(">",TJH_stacks);
		elseif (strfind(TJH_stacks, "!=") ~= nil) then
			argLogic = "<>";
			junk, TJH_stacks = strsplit("=",TJH_stacks);
		elseif (strfind(TJH_stacks, ">") ~= nil) then
			argLogic = ">";
			junk, TJH_stacks = strsplit(">",TJH_stacks);
		elseif (strfind(TJH_stacks, "<") ~= nil) then
			argLogic = "<";
			junk, TJH_stacks = strsplit("<",TJH_stacks);
		elseif (strfind(TJH_stacks, "=") ~= nil) then
			argLogic = "=";
			junk, TJH_stacks = strsplit("=",TJH_stacks);
	    elseif (strlen(strtrim(TJH_stacks)) ~= 0) then
			argLogic = "=";
		end

		TJH_stacks = tonumber(strtrim(TJH_stacks));
		argStacks = tonumber(argStacks);

		if (TJH_stacks == nil) then TJH_stacks = 0; end
		if (argStacks == nil) then argStacks = 0; end

		if (argEvent == "SPELL_AURA_APPLIED") then argStacks = 1; end

		if (((argLogic == "") and (TJH_stacks == 0)) or
		    (((argLogic == ">=") and (argStacks >= TJH_stacks))
			 or ((argLogic == "<=") and (argStacks <= TJH_stacks))
			 or ((argLogic == "<>") and (argStacks ~= TJH_stacks))
			 or ((argLogic == ">") and (argStacks > TJH_stacks))
			 or ((argLogic == "<") and (argStacks < TJH_stacks))
			 or ((argLogic == "=") and (argStacks == TJH_stacks))
			 )) then
			 doSpam = 1;
		end

		if (doSpam == 1) then
			if (argStacks == 1) then
				TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argSpellName..".", TJH_Channel);
			else
				TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argSpellName.." ("..argStacks..").", TJH_Channel);
			end;
		end;

		-- if (argEvent == "SPELL_AURA_APPLIED_DOSE") then
		--	if ((TJH_stacks == 0) or (tonumber(TJH_stacks) == tonumber(argStacks))) then
		--		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argSpellName.." ("..argStacks..").", TJH_Channel);
		--	end
		-- elseif ((TJH_stacks == 0) or (TJH_stacks == 1)) then
		--	TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argSpellName..".", TJH_Channel);
		-- end

		--	if (argEvent == "SPELL_AURA_APPLIED_DOSE") then
		--		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argSpellName.." ("..argStacks..").", TJH_Channel);
		--	elseif argMiss == "BUFF" then
		--		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argSpellName..".", TJH_Channel);
		--	elseif argMiss == "DEBUFF" then
		--		TJH_Print(argDestName.." is afflicted by "..argSpellName..".", TJH_Channel);
		--	end
		elseif (string_find(argEvent, "SPELL_AURA_REMOVED") ~= nil) then	-- to catch regular and _DOSE (add dose: argStacks/amount later?)
			TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." fades from "..argDestName..".", TJH_Channel);
		elseif (argEvent == "SPELL_AURA_DISPELLED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." dissipates from "..argDestName..".", TJH_Channel);
		elseif (argEvent == "SPELL_AURA_STOLEN") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." steals the "..argStacks.." "..strlower(argCritical)..".", TJH_Channel);
	-- double-check the output for these in the log
		elseif (argEvent == "SPELL_AURA_REFRESH") then
			TJH_wait(TJH_timeDelay,TJH_Print,argDestName.."'s "..argSpellName.." is refreshed.", TJH_Channel);
	-- double-check the output for these in the log
		elseif (argEvent == "SPELL_AURA_BROKEN") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." breaks the "..argSpellName.." "..strlower(argMiss)..".", TJH_Channel);
		elseif (argEvent == "SPELL_AURA_BROKEN_SPELL") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s " ..argStacks.." breaks "..argDestName.."s "..argSpellName..".", TJH_Channel);
--		elseif (argEvent == "SPELL_AURA_REFRESH") then
--			TJH_Print(argSourceName.." refreshes the "..argStacks.." "..strlower(argCritical)..".", TJH_Channel);
		else
			TJH_wait(TJH_timeDelay,TJH_Print,"The "..argSpellName.." "..strlower(argMiss).." triggered on "..argDestName..".", TJH_Channel);
		end
		return;
	elseif (string_find(argEvent, "SUMMON") ~= nil) then
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.. "'s "..argDestName.." summons "..argSpellName..".", TJH_Channel);
		return;
	elseif (argEvent == "SPELL_CREATE") then
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." creates "..argSpellName..".", TJH_Channel);
		return;
	elseif (string_find(argEvent, "SPELL_CAST_SUCCESS") ~= nil) and (string_find(argSpellName, "Create") ~= nil) then
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." casts "..argSpellName..".", TJH_Channel);
		return;
	elseif (string_find(argEvent, "SUCCESS") ~= nil) then
		if argDestName ~= "" then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." casts "..argSpellName.." at "..argDestName..".", TJH_Channel);
		else
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." casts "..argSpellName..".", TJH_Channel);
		end
		return;
	elseif (string_find(argEvent, "SPELL_CAST") ~= nil) then
		if argDestName ~= "" then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." casts "..argSpellName.." on "..argDestName..".", TJH_Channel);
		else
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.." casts "..argSpellName..".", TJH_Channel);
		end
		return;
	elseif (string_find(argEvent, "REMOVED") ~= nil) then
		TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." fades from "..argDestName..".", TJH_Channel);
		return;
	elseif string_find(argEvent, "MISSED") then
		if (argEvent == "SWING_MISSED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s swing misses ("..strlower(argSpellID)..").", TJH_Channel);	-- simplify?
		elseif (argEvent == "SPELL_MISSED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." misses ("..strlower(argMiss)..").", TJH_Channel);
		elseif (argEvent == "SPELL_PERIODIC_MISSED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." misses ("..strlower(argMiss)..").", TJH_Channel);
		elseif (argEvent == "RANGE_MISSED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." misses ("..strlower(argMiss)..").", TJH_Channel);
		elseif (argEvent == "DAMAGE_SHIELD_MISSED") then
			TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." misses ("..strlower(argMiss)..").", TJH_Channel);
		end
		return;
	elseif string_find(argEvent, "ENERGIZE") then
		TJH_PowerTypeParser();
		TJH_wait(TJH_timeDelay,TJH_Print,argDestName.." gains "..argMiss.. " "..TJH_powerType.." from "..argSourceName.."'s "..argSpellName..".", TJH_Channel);
		return;
	elseif string_find(argEvent, "LEECH") then
		TJH_PowerTypeParser();
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." drains "..argMiss.. " "..TJH_powerType.." from "..argDestName..".", TJH_Channel);
		return;
	elseif string_find(argEvent, "DRAIN") then
		TJH_PowerTypeParser();
		TJH_wait(TJH_timeDelay,TJH_Print,argSourceName.."'s "..argSpellName.." drains "..argMiss.. " "..TJH_powerType.." from "..argDestName..".", TJH_Channel);
		return;
	elseif (string_find(argEvent, "SPELL") ~= nil) then
		TJH_wait(TJH_timeDelay,TJH_Print,argSpellName.." cast.", TJH_Channel);
		return;
	elseif argEvent ~= "" then
		TJH_wait(TJH_timeDelay,TJH_Print,"TJH: ("..strlower(argEvent)..") just happened.", TJH_Channel);
		return;
	else
--		TJH_Print("TJH: Shake and bake!", TJH_Channel);	--debugging
	end
end


function TJH_SchoolTypeParser()
	TJH_spellSchool = "";
	if argEvent == "SWING_DAMAGE" then
		if argSchool == 1 then
			TJH_spellSchool = "physical";
			return;
		elseif argSchool == 2 then
			TJH_spellSchool = "holy";
			return;
		elseif argSchool == 4 then
			TJH_spellSchool = "fire";
			return;
		elseif argSchool == 8 then
			TJH_spellSchool = "nature";
			return;
		elseif argSchool == 16 then
			TJH_spellSchool = "frost";
			return;
		elseif argSchool == 20 then
			TJH_spellSchool = "frostfire";
			return;
		elseif argSchool == 32 then
			TJH_spellSchool = "shadow";
			return;
		elseif argSchool == 64 then
			TJH_spellSchool = "arcane";
			return;
		end
	else
		if argAbsorbed == 1 then
			TJH_spellSchool = "physical";
			return;
		elseif argAbsorbed == 2 then
			TJH_spellSchool = "holy";
			return;
		elseif argAbsorbed == 4 then
			TJH_spellSchool = "fire";
			return;
		elseif argAbsorbed == 8 then
			TJH_spellSchool = "nature";
			return;
		elseif argAbsorbed == 16 then
			TJH_spellSchool = "frost";
			return;
		elseif argAbsorbed == 20 then
			TJH_spellSchool = "frostfire";
			return;
		elseif argAbsorbed == 32 then
			TJH_spellSchool = "shadow";
			return;
		elseif argAbsorbed == 64 then
			TJH_spellSchool = "arcane";
			return;
		end
	end
end

function TJH_PowerTypeParser()
	if (string_find(argEvent, "DRAIN") ~= nil) or (string_find(argEvent, "LEECH") ~= nil) or (string_find(argEvent, "ENERGIZE") ~= nil) then
		if argStacks == -2 then
			TJH_powerType = "health";
			return;
		elseif argStacks == 0 then
			TJH_powerType = "mana";
			return;
		elseif argStacks == 1 then
			TJH_powerType = "rage";
			return;
		elseif argStacks == 2 then
			TJH_powerType = "focus";
			return;
		elseif argStacks == 3 then
			TJH_powerType = "energy";
			return;
		elseif argStacks == 4 then
			TJH_powerType = "pet happiness";
			return;
		elseif argStacks == 5 then
			TJH_powerType = "runes";
			return;
		elseif argStacks == 6 then
			TJH_powerType = "runic power";
			return;
		end
	end
end



-- Function to insert rule into the table (data assumed good)
function TJH_addRule(eventname, sourcename, destname, spellname, affflag, outputchann, rulespam, timedelay, stacks)
	table.insert(TJH_Rules, eventname..";"..sourcename..";"..destname..";"..spellname..";"..affflag..";"..strupper(outputchann)..";"..rulespam..";"..timedelay..";"..stacks);
	TJH_ListRules();
end



--function to get single rule from GUI and pass it on to delete function
function TJH_GUIDeleteRule()
	local count=#(TJH_Rules);
	TJH_delrule=TJH_IndivRuleBox:GetText();
	TJH_DeleteRule(TJH_delrule);
end



--function to delete rule
function TJH_DeleteRule(TJH_delrule)
	local count=#(TJH_Rules);
	local ruletodelete=tonumber(TJH_delrule);
	if (count == 0) then
		TJH_Print(red.."TJH: There are no rules defined.", "SELF");
	elseif ruletodelete == "" then
		TJH_Print(red.."TJH: Please specify a Rule # to delete. /tjh list may help you out here.", "SELF");
	else
		table.remove(TJH_Rules,ruletodelete);
		TJH_Print(orange.."TJH: Custom Rules were updated!", "SELF")
		TJH_ListRules();
	end
end



--function to delete all rules on button press
function TJH_ClearRules()
	for k in pairs(TJH_Rules) do TJH_Rules[k]=nil; end
	TJH_Rules={};
	TJH_Print(yellow.."TJH: Event rules deleted.", "SELF")
end


-- function to list all rules on button press
function TJH_ListRules()
	local count=#(TJH_Rules);
	if (count == 0) then
		TJH_Print(orange.."TJH: There are no rules defined.", "SELF");
	else
		TJH_Print(orange.."TJH: Rule #: event;source;target;spellname;affiliation;channel;spam;delay", "SELF");
		for i=1,count do
		TJH_Print("TJH: Rule "..i..": "..TJH_Rules[i], "SELF");
		end
	end
end


-- function to add delay to action; check sound delay
local waitTable = {};
local waitFrame = nil;
function TJH_wait(delay, func, ...)
--  if(type(delay) ~= "number" or type(func) ~= "function") then
--    return false;
--  end
	if (delay == "" or delay == nil) then delay = 0; end;
	if(type(delay) ~= "number") then delay = tonumber(delay); end

	if(waitFrame == nil) then
		waitFrame = CreateFrame("Frame", "WaitFrame", UIParent);
		waitFrame:SetScript("onUpdate", function (self, elapse)
		local count = #waitTable;
		local i = 1;
			while(i <= count) do
				local waitRecord = tremove(waitTable, i);
				local d = tremove(waitRecord, 1);
				local f = tremove(waitRecord, 1);
				local p = tremove(waitRecord, 1);
				if(d > elapse) then
					tinsert(waitTable, i, {d-elapse, f, p});
					i = i + 1;
				else
					count = count - 1;
					f(unpack(p));
				end
			end
		end);
	end
	tinsert(waitTable,{delay,func,{...}});
	return true;
end


function TJH_Print(msg, channel)
	if channel == nil then
		DEFAULT_CHAT_FRAME:AddMessage(orange.."TJH: "..white.."channel=SELF/WARN/PARTY/RAID/RAID_WARNING/EMOTE/PLAYSOUND/PlayerName/ChannelName/auto"..orange.." to set output channel");
		DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
	end

	if channel == "AUTO" then
		if (IsInInstance() or  IsInGroup(LE_PARTY_CATEGORY_INSTANCE)) then
			SendChatMessage(msg, "INSTANCE_CHAT");
		elseif (IsInRaid()) then
			SendChatMessage(msg, "RAID");
		elseif (IsInGroup()) then
			SendChatMessage(msg, "PARTY");
		else
			DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
		end
	elseif channel == "SELF" then
		DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
	elseif channel == "WARN" then
		DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
		UIErrorsFrame:AddMessage(msg, 1.0, 1.0, 0.0, 1.0, UIERRORS_HOLD_TIME);
	elseif channel == "RAID" then
		if (IsInRaid()) then
			SendChatMessage(msg, channel);
		else
			DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
		end
	elseif channel == "RAID_WARNING" or channel == "RW" or channel == "RS" then
		SendChatMessage(msg, "RAID_WARNING");
	elseif channel == "PARTY" then
		if (IsInGroup()) then
			SendChatMessage(msg, channel);
		else
			DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
		end
	elseif channel == "SAY" then
		SendChatMessage(msg, channel);
	elseif channel == "EMOTE" then
		if string_find(msg, "/") then
			msg = string_sub(msg, 2)
			DoEmote(msg);
		else
			SendChatMessage(msg, channel);
		end
-- allows the playing of soundfiles: pathnames in the format of "Sound\Music\GlueScreenMusic\wow_main_theme.mp3" but not ("Sound\\Music\\GlueScreenMusic\\wow_main_theme.mp3"; will play mp3 and wav files but cannot be stopped mid-file, so choose wisely
	elseif channel == "PLAY" or channel == "SOUND" or channel == "PLAYSOUND" then
		PlaySoundFile(msg);
	elseif channel == "YELL" then
		SendChatMessage(msg, channel);
	elseif channel == "GUILD" then
		SendChatMessage(msg, channel);
	elseif channel == "" then
		DEFAULT_CHAT_FRAME:AddMessage(yellow..msg);
	elseif GetChannelName(channel) > 0 then
			SendChatMessage(msg, "CHANNEL", nil, GetChannelName(channel));
-- allows use of dynamic target/source variables in output spam; for ex. will auto-whisper target regardless of name, defaults to SELF if it can't find a name
	elseif channel == "*SOURCE" then
		channel = argSourceName;
		if channel == "" then channel = "SELF";
		else
			SendChatMessage(msg, "WHISPER", nil, channel);
		end
	elseif channel == "*TARGET" then
		channel = argDestName;
		if channel == "" then channel = "SELF";
		else
			SendChatMessage(msg, "WHISPER", nil, channel);
		end

	else
		SendChatMessage(msg, "WHISPER", nil, channel);
	end
end



-- GUI support
function TJH_Enablee()
	local gtext = TJH_MainFormButtonEnable:GetText();
	if gtext == "Disabled" then
		TJH_MainFormButtonEnable:SetNormalFontObject("GameFontNormal");
		TJH_MainFormButtonEnable:SetText("*Enabled*");
		TJH_Options.enabled = 1;
	elseif gtext == "*Enabled*" then
		TJH_MainFormButtonEnable:SetNormalFontObject("GameFontDisable");
		TJH_MainFormButtonEnable:SetText("Disabled");
		TJH_Options.enabled = 0;
	end
end


function TJH_EnableButton()
	if TJH_Options.enabled == 0 then
		TJH_MainFormButtonEnable:SetNormalFontObject("GameFontDisable");
		TJH_MainFormButtonEnable:SetText("Disabled");
	elseif TJH_Options.enabled == 1 then
		TJH_MainFormButtonEnable:SetNormalFontObject("GameFontNormal");
		TJH_MainFormButtonEnable:SetText("*Enabled*");
	end
end



--Update the window info
function TJH_GUIRefresh()
--	TJH_ChannelBox:SetText(strlower(TJH_Channel));
	TJH_RuleEventBoxText:SetText("");
	TJH_RuleAffiliationBoxText:SetText("");
	TJH_IndivRuleBox:SetText("");
	TJH_TimeDelayBox:SetText("");
end


--function to add channel to GUI and properly update text if custom channel
function TJH_CurrentChannel()
	if TJH_AutoChannelBox:GetChecked() == nil then
--		if TJH_Channel == "WHISPER" then	-- code to load current channel into gui on show, but it's a bit annoying
--			TJH_ChannelBox:SetText(TJH_whisperTarget);
----		elseif TJH_ChannelTarget == nil then
----			TJH_ChannelBox:SetText(string.lower(TJH_Channel));
--		else
--			if TJH_Channel == nil then
--				TJH_ChannelBox:SetText("self");
--			else
--				if TJH_Channel == "AUTO" then
--					TJH_ChannelBox:SetText("self");
--				else
--					TJH_ChannelBox:SetText(strlower(TJH_Channel));
--				end
--			end
--		end
		TJH_ChannelBox:SetText("SELF");	-- replaced above with "self" on show for simplicity and reduced public spam; consider restoring  if it gets annoying not having previous channel name after gui close

	elseif TJH_AutoChannelBox:GetChecked() ~= nil then
		TJH_GUIAutoChannel();
	end
end


--supports gui channel auto select
function TJH_GUIAutoChannel()
	local isChecked = TJH_AutoChannelBox:GetChecked();
	if isChecked == 1 then
		TJH_Channel = "auto";
		TJH_ChannelBox:SetText("auto");
	elseif isChecked ~= 1 then
		TJH_ChannelBox:SetText("SELF");
	end
end


-- function to update master channel from GUI, if I decide to have a master channel
--function TJH_AutoUpdate()
--	if TJH_AutoChannelBox:GetChecked() == nil then
--		if TJH_ChannelBox:GetText() == nil or TJH_ChannelBox:GetText() == "" then
--			local channelString = "channel,SELF";
--			TJH_SlashCMD(channelString);
--		else
--			local channelString = ("channel,"..TJH_ChannelBox:GetText());
--			TJH_SlashCMD(channelString);
--		end
--	elseif TJH_AutoChannelBox:GetChecked() == 1 then
--		TJH_GUIAutoChannel();
--		TJH_Print(orange.."TJH: Channel set to ["..TJH_Channel.."].","SELF");
--	end
--end


-- function to add a rule via GUI
function TJH_GUIAddCustomRule()
	local guievent = "";
	if TJH_RuleEventManualBox:GetText() ~= "" then guievent = TJH_RuleEventManualBox:GetText(); else guievent = TJH_RuleEventBoxText:GetText(); end
	local guisource = TJH_RuleSourceBox:GetText();
	local guitarget = TJH_RuleTargetBox:GetText();
	local guispell = TJH_RuleSpellnameBox:GetText();
	local guistacks = TJH_RuleStacksBox:GetText();
	local guiaff = TJH_RuleAffiliationBoxText:GetText();
	local guichann = strupper(TJH_ChannelBox:GetText());
	local guispam = strtrim(TJH_RuleSpamBox:GetText());
	local guidelay = strtrim(TJH_TimeDelayBox:GetText());
	local guistacks = strtrim(TJH_RuleStacksBox:GetText());
	if guievent == nil then guievent = ""; end
	if guisource == nil then guisource = ""; end
	if guitarget == nil then guitarget = ""; end
	if guispell == nil then guispell = ""; end
	if guiaff == nil then guiaff = ""; end
	if guichann == nil then guichann = "SELF"; end
	if guispam == nil then guispam = ""; end
	if guidelay == nil then guidelay = ""; end
	if guistacks == nil then guistacks = ""; end
	local ruleString = ("rule "..guievent..";"..guisource..";"..guitarget..";"..guispell..";"..guiaff..";"..guichann..";"..guispam..";"..guidelay..";"..guistacks);
	TJH_SlashCMD(ruleString);
	-- clear most rule fields on Add Rule
	TJH_ClearGUI();
end


-- function to load rule into gui
function TJH_LoadRule()
	local count=#(TJH_Rules);
	local loadrule=tonumber(TJH_IndivRuleBox:GetText());
	local i = loadrule;
	if (count == 0) then
		TJH_Print(red.."TJH: There are no rules defined.", "SELF");
	elseif i == nil then
		TJH_Print(red.."TJH: Please specify a Rule # to load. /tjh list may help you out here.", "SELF");
	elseif i > count then
		TJH_Print(red.."TJH: There is no such rule. /tjh list may help you out here.", "SELF");
	elseif loadrule == "" then
		TJH_Print(red.."TJH: Please specify a Rule # to load. /tjh list may help you out here.", "SELF");
	else
		local ruleString = TJH_Rules[loadrule];
		local levent, lsource, ldest, lspell, laff, lchannel, lspam, ldelay, lstacks = strsplit(";", ruleString);
		if levent == nil then levent = ""; end
		if lsource == nil then lsource = ""; end
		if ldest == nil then ldest = ""; end
		if lspell == nil then lspell = ""; end
		if laff == nil then laff = ""; end
		if lchannel == nil then lchannel = "SELF"; end
		if lspam == nil then lspam = ""; end
		if ldelay == nil then ldelay = ""; end
		if lstacks == nil then lstacks = ""; end
		TJH_RuleEventManualBox:SetText(levent);
		TJH_RuleEventBoxText:SetText("");
		TJH_RuleSourceBox:SetText(lsource);
		TJH_RuleTargetBox:SetText(ldest);
		TJH_RuleSpellnameBox:SetText(lspell);
		TJH_RuleAffiliationBoxText:SetText(laff);
		TJH_ChannelBox:SetText(lchannel);
		TJH_RuleSpamBox:SetText(lspam);
		TJH_TimeDelayBox:SetText(ldelay);
		TJH_RuleStacksBox:SetText(lstacks);
	end
end


-- function to clear the GUI
function TJH_ClearGUI()
	TJH_RuleEventManualBox:SetText("");
	TJH_RuleEventBoxText:SetText("");
	TJH_RuleSourceBox:SetText("");
	TJH_RuleTargetBox:SetText("");
	TJH_RuleSpellnameBox:SetText("");
	TJH_RuleStacksBox:SetText("");
	TJH_RuleAffiliationBoxText:SetText("");
	TJH_ChannelBox:SetText("");
	TJH_RuleSpamBox:SetText("");
	TJH_IndivRuleBox:SetText("");
	TJH_TimeDelayBox:SetText("");
end


-- dropdown table contents
TJHevent_menuList = {

   {
        text = "",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "DAMAGE_SHIELD",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "DAMAGE_SHIELD_MISSED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "ENCHANT_APPLIED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "ENCHANT_REMOVED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "ENVIRONMENTAL_DAMAGE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "PARTY_KILL",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "RANGE_DAMAGE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "RANGE_MISSED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_AURA_APPLIED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_AURA_BROKEN",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_AURA_REFRESH",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_AURA_REMOVED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_AURA_STOLEN",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_BUILDING",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_CAST_FAILED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_CAST_START",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_CAST_SUCCESS",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_DAMAGE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_DISPEL",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_DISPEL_FAILED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_DRAIN",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_DURABILITY_DAMAGE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_ENERGIZE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_EXTRA_ATTACKS",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_HEAL",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_INSTAKILL",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_INTERRUPT",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_LEECH",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_MISSED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_PERIODIC_DAMAGE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_PERIODIC_DRAIN",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_PERIODIC_ENERGIZE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_PERIODIC_HEAL",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_PERIODIC_LEECH",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SPELL_PERIODIC_MISSED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SWING_DAMAGE",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "SWING_MISSED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "UNIT_DESTROYED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    },
   {
        text = "UNIT_DIED",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleEventBox, self:GetID(), 0);  end;
    }

}

TJHclass_menuList = {

   {
        text = "",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "self",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "party",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "raid",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "friend",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "enemy",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "target",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    },
   {
        text = "focus",
        func = function(self) UIDropDownMenu_SetSelectedID(TJH_RuleAffiliationBox, self:GetID(), 0);  end;
    }

}

--TJHprofile_menuList = {
--
--   {
--        text = "",
--        func = function(self) UIDropDownMenu_SetSelectedID(TJH_ProfileBox, self:GetID(), 0);  end;
--    }
--
--
--}
