****ThatJustHappened ReadMe****
Table of Contents:
..Introduction
..Slash Commands
..Valid Source/Target Names
..Channel Options
..Affiliation
..Delayed Triggers
..Examples
..Sample Events
..To Check the CombatLog for Event Names
..Troubleshooting
..To Do/Known Issues


** Introduction **
This mod is an event tracker that will watch for a given event and pop up a warning in chat or on screen. It is similar to SCT or EavesDrop, but lets you spam customized events to the channel of your choice. If you want something to spam *you* when events happen, feel free to use one of those other mods. If you want to be able to set up warnings for any combat event and announce it pretty much anywhere, this mod's for you.

To get started, first set up some rules to monitor the events you want to watch. There are a lot of ways to choose what you want to see, including the event's source, the event's target, the watched-for spellname, the affiliation or category of events you want to watch, and the channel where the warning is sent. To see all of any of these things, just leave it blank. You can also use partial event/source/target/spell names to cast a wider net (ex: AURA), or full names to limit triggers (ex: SPELL_AURA_REMOVED). (See slash commands and examples below.) Rules are additive, so using a rule showing all "self" events with one showing all "raid" events will result in two lines per event. Rules and enable settings are saved across sessions by character (initial default is "off" but it will stay on until you turn it off).

For example, set the "affiliation" dropdown box to "target" and the output channel to "SELF" (leave the other rule options blank). Hit the "Add Rule" button; you'll see your new rule printed in the chat window. Toggle the bottom-left button from "Disabled" to "*Enabled*". Now whenever you target a player or a mob, their combat events will be output to your chat window. 


**Slash Commands**
/tjh or /tjh gui for GUI
/tjh help for this list
/tjh on (to activate)
/tjh off (to deactivate)
/tjh rule EVENT_NAME;SourceName;TargetName;SpellName;Affiliation;Channel;Spam;Delay;Stacks
/tjh list (to list current rules)
/tjh delete (to delete all rules)
/tjh delete;3 (to delete the third rule)
/tjh reset (deletes all rules, resets output to SELF)


**Valid Source/Target Names**
Player names, NPC names, and object names as seen in the in-game tooltips. Also accepts the active target or focus at that time.


**Channel Options**
Channel settings: SELF/WARN/PARTY/RAID/RAID_WARNING/RW/RS/YELL/PlayerName/ChannelName/auto/EMOTE/PLAYSOUND/*source/*target (to set output channel to SELF, RAID, whisper a named player, whisper the event source or target, or use a custom channel, etc.) SELF is the default channel. Enabling the "Auto-Select Channel" checkbox sets the rule channel to "auto"; rules with "auto" as the channel will be output to self, party, or raid chat depending on your group status. Dynamic source/target options default to SELF if no usable whisper target can be found.

Only you will be able to hear the sound files play. Add "PLAYSOUND" to the channel box and a soundfile pathname to the output box. Works for pathnames in the format of "Sound\Music\GlueScreenMusic\wow_main_theme.mp3" but not "Sound\\Music\\GlueScreenMusic\\wow_main_theme.mp3"; will play mp3 and wav files on event but cannot be stopped mid-playback, so choose wisely (see http://www.wowwiki.com/API_PlaySoundFile for sound file options)
Example: 
/tjh rule SPELL_AURA_APPLIED;Belleboom;;Arcane Intellect;;PLAYSOUND;Sound\Creature\NPCGhoul\NPCGhoulVendor04.wav;

Channel settings are on a "by rule" basis; each event is output to the specified channel. For example the first rule spams RAID when anyone takes Spout damage, the second alters a custom mage channel when a stealable buff is applied: 
/tjh rule ;Lurker;;Spout;;RAID;;
/tjh rule AURA_APPLIED;Greyheart Nether-Mage;;Arcane Destruction;;GDMAGES;;


**Custom Rule Spam**
Basics: You have the option to set custom spam text for each rule, which replaces the standard combat log-type output. For example, if Savar is my main tank:
/tjh rule SPELL_AURA_APPLIED;;Savar;Mortal Strike;;RW;Incoming heals on Savar reduced by 50%!;

Dynamic Variables: using any of the following in your spam will substitute the current event's variable, or blank if that variable is nil:
*event = current event's name
*source = current event's source
*target = current event's target
*spell = current event's spellname
-- Note: depending on the event, these variables may not always give you what you expect, but they can be great for rules like the following:
/tjh rule UNIT_DIED;;;;raid;RW;*target is down!;

It is also possible to use dynamic source/target variables in output channels, so that adding "*target" to the spam output option will whisper the event target whenever triggered; "*source" is also an option.


**Affiliation**
The last rule option can be used to filter out all events except those that affect either self/party/raid/friend/enemy/target/focus. So if you want to see every time a buff expires on your current focus (whatever that is), use "/tjh rule SPELL_AURA_REMOVED;;;;focus;;;" (Note: this would also trigger if your focus removed a buff from another mob.) Pets/guardians/other have been removed from the "raid" affiliation on request, but are still included in the "friendly" affiliation. 


**Examples**
* Basic Rule Structure: event;source;target;spell;affiliation;channel;spam;delay

To track all interrupts on a specific target and splash it up on your screen:
*/tjh rule INTERRUPT;;TargetNameHere;;;WARN;;

To announce all kills made by party members and output to party chat:
*/tjh rule PARTY_KILL;;;;;PARTY;;

To alert yourself to all buff-related events on your current target:
*/tjh rule AURA;;;;target;SELF;;

To announce custom spam in raid chat when your target summons a Tremor Totem:
*/tjh rule SUMMON;target;Tremor Totem;;;RAID;Totem up!;

To track all events on a player done by your target, and whisper them to that player:
*/tjh rule ;PlayerName;;;target;PlayerName;;

Announces in the custom channel "gdmages" when Greyheart Nether-Mage gains any of the three spell-stealable Destruction buffs:
*/tjh rule AURA_APPLIED;Greyheart Nether-Mage;;Destruction;;GDMAGES;Steal that spell!;

Spams raid warning when Greyheart Tidecaller summons a water elemental:
*/tjh rule SUMMON;Greyheart Tidecaller;;Water Elemental Totem;;RW;;

Tracks expiring weapon oil on my weapon and alerts me with a custom message:
*/tjh rule ENCHANT_REMOVED;;Belleboom;;Oil;;SELF;Time for an oil change.;

Tracks all expiring buffs on your focus and alerts me via splash screen:
*/tjh rule SPELL_AURA_REMOVED;;;;focus;WARN;;

To see all events (warning, lots of spam), output to the default channel "SELF":
*/tjh rule ;;;;;;;

To announce in RAID when anyone is hit by The Lurker Below's Spout damage:
*/tjh rule ;Lurker;;Spout;;RAID;;

To announce whenever Misdirection fades from a specified PlayerName:
*/tjh rule SPELL_AURA_REMOVED;;PlayerName;Misdirection;;SAY;Misdirection finished.;

To alert the healers when the tank is knocked down (works on both Hyjal trash and Zul'aman Axe Throwers):
*/tjh rule SPELL_AURA_APPLIED;;TankName;Knockdown;;SAY;*target knocked down!;

To alert yourself when Sacred Shield expires (using spell id instead of name for greater specificity):
/tjh rule SPELL_AURA_REMOVED;YourName;;53601;SELF;SELF;Sacred Shield fell off.

To alert the raid when an Amani'shi Medicine Man drops his Protective Ward (immunity) totem:
*/tjh rule SUMMON;;Protective Ward;;;RW;Kill the totem!;

These together will announce when Halazzi's Saber Lash does damage, and when it is dodged, parried, or misses.
*/tjh rule SPELL_DAMAGE;Halazzi;;Saber Lash;;RAID;;
*/tjh rule SPELL_MISSED;Halazzi;;Saber Lash;;RAID;;


**Delayed Triggers**
It is also possible to specify an optional delay in event spam (so any spam triggers X seconds after the event happens. This can be specified using the GUI or via slash command, as follows:
*/tjh rule SPELL_AURA_APPLIED;Belleboom;;;;WARN;*event happened 5 seconds ago!;5

If you don't include a number for the delay it will default to "0".


**Sample Events (See http://www.wowwiki.com/API_COMBAT_LOG_EVENT for more details)**
PARTY_KILL
SWING_DAMAGE
SPELL_DAMAGE
SPELL_AURA_REMOVED
SWING: These events relate to melee swings, commonly called �White Damage�.
RANGE: These events relate to hunters shooting their bow or a warlock/mage shooting their wand.
SPELL: These events relate to spells and abilities.
SPELL_AURA: These events relate to buffs and debuffs.�
AURA_APPLIED
SPELL_PERIODIC: These events relate to HoT, DoTs and similar effects.�
SPELL_CREATE
SPELL_AURA_REMOVED
SPELL_CAST: These events relate to spells starting and failing.�
SPELL_CAST_START
SPELL_CAST_SUCCESS
DAMAGE_SHIELD: These events relate to damage shields, such as Thorns�
ENCHANT: These events relate to temporary and permanent item buffs.�
ENVIRONMENTAL: This is any damage done by the world. Fires, Lava, Falling, etc.
DAMAGE: If the event resulted in damage, here it is.�
MISSED: If the event resulted in failure, such as missing, resisting or being blocked.�
HEAL: If the event resulted in a heal.�
ENERGIZE: If the event resulted in a power restoration.�
LEECH: If the event transferred health or power.�
DRAIN: If the event reduces power, but did not transfer it.
PARTY_KILL: Fired when you or a party member kills something.�
UNIT_DIED: Fired when any nearby unit dies.
SUCCESS
UNIT_DIED
UNIT_DESTROYED
FAILED
INTERRUPT
SPELL_SUMMON
SUMMON


**To Check the CombatLog for Event Names**
If you want to find the name of an unknown event, turn on combat logging in game by typing:
/combatlog

Go out and do whatever it is you want to investigate, log out, and check the WoWCombatLog.txt file in your WoW/Logs folder. It's complex, but will look something like this:
5/15 15:05:57.383, SPELL_INTERRUPT,0x000000000040B0C7,"Belleboom",0x511,0xF1300048121297E0,"Shienor Sorcerer",0x10a48,2139,"Counterspell",0x40,26098,"Lightning Bolt",8

What you are looking for is the second block in all caps, in this case "SPELL_INTERRUPT". In this example, "Belleboom" is the source, "Shienor Sorcerer" is the target, and "Counterspell" is the spell name.


**Troubleshooting**
If nothing is being announced: 
-- open the GUI using "/tjh" and click the "Add Rule" button (if nothing is specified it will watch everything);
-- make sure the toggle button at the bottom left reads "*Enabled*" (click on it if it says "Disabled")
-- do something that should trigger an event, such as spell casting, feeding your pet, or taking damage


**To Do/Known Issues**
-- some actions have multiple events (ex: fishing = "casts" + "creates" + "gains"); to avoid overspam you can (for ex.) watch only SPELL_AURA_APPLIED instead of SPELL_CAST_SUCCESS
-- some events aren't exactly what you'd expect; a sheep break event has no event source attached, so searching for "/tjh rule SPELL_AURA_REMOVED;Belleboom;;Polymorph;;;;" to see my sheep break won't work. Instead; I'd make the mob my focus and use "/tjh rule SPELL_AURA_REMOVED;;;;focus;;;"
-- GUILD is a valid channel but undocumented as it probably shouldn't be used (unless you want to be /gkicked? spam potential with this mod is very high:p)
-- trying to delete an invalid rule number will delete the last rule
-- considering rule management gui
-- TJH gives an error when right-clicking a player frame to "set/clear focus". Focus can still be set or cleared using commands such as "/target focus" or "/clearfocus"; this is a known issue with UIDropDownMenu_Initialize but I'll continue to work on it 